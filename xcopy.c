/*
 * xcopy: quickly pipe text data into, or out of, the primary X
 * selection
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include <unistd.h>

#include <X11/X.h>
#include <X11/Intrinsic.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

bool init_X(void);
void run_X(void);
void done_X(void);
void full_redraw(void);
void do_paste(Window window, Atom property, bool cutbuffer);

char *pname;			       /* program name */

void error (char *fmt, ...);

/* set from command-line parameters */
char *display = NULL;
enum { STRING, CTEXT, UTF8, HTML, TARGETS, TIMESTAMP, CUSTOM } mode = STRING;
bool use_clipboard = false;
const char *custom_selection = NULL;
bool drop = false;
bool oneshot = false;
const char *custom_seltype = NULL;
const char *input_file_name, *output_file_name;
FILE *output_fp = NULL;
bool fork_when_writing = true;
bool use_cutbuffers = true;
bool use_outgoing_incr = true;
int sel_delta = 16384;

/* selection data */
char *seltext = NULL;
int sellen, selsize;
bool already_got_seltext = false;

/* used in drag and drop */
bool still_grabbed = false;
Window drop_receiver = None, drop_proxy = None;
bool waiting_for_xdndstatus = false;
bool last_xdndstatus_was_positive = false;

/* incremental transfers still pending when we return to the event loop */
struct incr {
    Window window;
    Atom property;
    Atom type;
    int format;
    unsigned char *data;
    size_t size;
} *incrs = NULL;
int nincrs = 0, incrsize = 0;

/* used to implement oneshot */
bool pasted_something = false;

/* functional parameters */
bool reading = false;                  /* read instead of writing? */
bool convert_to_ctext = true;          /* Xmb convert to compound text? */
bool convert_utf16_utf8 = false;       /* for -h */
bool verbose = false;

const char usagemsg[] =
    "usage: xcopy [ -r ] [ -u | -c ] [ -C ]\n"
    "modes: -r       read X selection and print on stdout\n"
    "       no -r    write to X selection from stdin\n"
    " read: -t       get the list of targets available to retrieve\n"
    "       -T       get the time stamp of the selection contents\n"
    "       -a atom  get an arbitrary form of the selection data\n"
    "       -v       verbosely report progress in reading selection\n"
    "       -o file  write retrieved data to file instead of stdout\n"
    "write: -F       remain in foreground until selection reclaimed\n"
    "       -1       one-shot mode: exit immediately after first paste\n"
    "       -I       do not attempt incremental transfers (INCR)\n"
    "       -D file  drag-and-drop path to file as a text/uri-list\n"
    "       --drop   drag-and-drop arbitrary data of your choice\n"
    "       file     read input data from file instead of stdin\n"
    " both: -u       work with UTF8_STRING type selections\n"
    "       -c       work with COMPOUND_TEXT type selections\n"
    "       -H       work with text/html type selections (which are in UTF-16)\n"
    "       -h       work with text/html, but autoconvert to/from UTF-8\n"
    "       -C       suppress automatic conversion to COMPOUND_TEXT\n"
    "       -b       use the CLIPBOARD rather than the PRIMARY selection\n"
    "       -S atom  use an arbitrarily named selection (e.g. SECONDARY)\n"
    "       -d size  transfer data in chunks of at most <size> (default 16384)\n"
    "       -B       do not use root window cut buffers\n"
    " also: xcopy --version              report version number\n"
    "       xcopy --help                 display this help text\n"
    "       xcopy --licence              display the (MIT) licence text\n"
    ;

void usage(void) {
    fputs(usagemsg, stdout);
}

const char licencemsg[] =
    "xcopy is copyright 2001-2004,2008 Simon Tatham.\n"
    "\n"
    "Permission is hereby granted, free of charge, to any person\n"
    "obtaining a copy of this software and associated documentation files\n"
    "(the \"Software\"), to deal in the Software without restriction,\n"
    "including without limitation the rights to use, copy, modify, merge,\n"
    "publish, distribute, sublicense, and/or sell copies of the Software,\n"
    "and to permit persons to whom the Software is furnished to do so,\n"
    "subject to the following conditions:\n"
    "\n"
    "The above copyright notice and this permission notice shall be\n"
    "included in all copies or substantial portions of the Software.\n"
    "\n"
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\n"
    "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n"
    "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n"
    "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS\n"
    "BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN\n"
    "ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN\n"
    "CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n"
    "SOFTWARE.\n"
    ;

void licence(void) {
    fputs(licencemsg, stdout);
}

void version(void) {
    printf("xcopy, unknown version\n"); /*---buildsys-replace---*/
}

int main(int ac, char **av) {
    int n;
    bool eventloop, doing_args = true;

    pname = *av;

    /* parse the command line arguments */
    while (--ac > 0) {
	const char *p = *++av;

        if (!doing_args || *p != '-') {
            if (!input_file_name) {
                input_file_name = p;
            } else {
                error ("unexpected parameter `%s'", p);
            }
        } else if (!strcmp(p, "--")) {
            doing_args = false;
        } else if (!strcmp(p, "-display") || !strcmp(p, "-disp")) {
	    if (!av[1])
		error ("option `%s' expects a parameter", p);
	    display = *++av, --ac;
        } else if (p[1] != '-') {
            /*
             * Parse this argument as a concatenation of single-letter
             * options, terminated by either end-of-string or the
             * first option that takes a value.
             */
            while (*p) {
                char c = *p++;
                switch (c) {
                    break;
                  case 'r':
                    reading = true;
                    break;
                  case 'u':
                    mode = UTF8;
                    break;
                  case 'H':
                    mode = HTML;
                    break;
                  case 'h':
                    mode = HTML;
                    convert_utf16_utf8 = true;
                    break;
                  case 'c':
                    mode = CTEXT;
                    break;
                  case 'C':
                    convert_to_ctext = false;
                    break;
                  case 'b':
                    use_clipboard = true;
                    custom_selection = NULL;
                    break;
                  case 't':
                    mode = TARGETS;
                    break;
                  case 'T':
                    mode = TIMESTAMP;
                    break;
                  case 'I':
                    use_outgoing_incr = false;
                    break;
                  case 'B':
                    use_cutbuffers = false;
                    break;
                  case 'F':
                    fork_when_writing = false;
                    break;
                  case '1':
                    oneshot = true;
                    break;
                  case 'v':
                    verbose = true;
                    break;
                  case 'S':
                  case 'a':
                  case 'd':
                  case 'D':
                  case 'o': {
                    /* Single case for options taking an argument, so
                     * we can share the logic of where to find the
                     * argument */
                    const char *arg;
                    if (*p) {
                        arg = p;
                        p = ""; /* move on to next argv word after this */
                    } else if (--ac > 0) {
                        arg = *++av;
                    } else {
                        error("expected an argument to `-%c'", c);
                    }

                    /* Now handle the argument depending on which option */
                    switch (c) {
                      case 'S':
                        use_clipboard = false;
                        custom_selection = arg;
                        break;
                      case 'a':
                        custom_seltype = arg;
                        mode = CUSTOM;
                        break;
                      case 'd':
                        sel_delta = atoi(arg);
                        break;
                      case 'D':
                        drop = true;
                        fork_when_writing = false;
                        custom_seltype = "text/uri-list";
                        mode = CUSTOM;

                        /* Create the input selection data by URL-encoding the
                         * absolute path name of the supplied file. */

                        char *resolved = realpath(arg, NULL);
                        if (!resolved)
                            error("%s: realpath: %s", arg, strerror(errno));

                        selsize = 3 * strlen(resolved) + 20;
                        seltext = malloc(selsize);
                        if (!seltext)
                            error ("out of memory");
                        sellen = 0;

                        sellen += sprintf(seltext + sellen, "file://");

                        for (const char *p = resolved; *p; p++) {
                            static const char safe_chars[] =
                                "-./_0123456789"
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz";

                            if (strchr(safe_chars, *p)) {
                                seltext[sellen++] = *p;
                            } else {
                                sellen += sprintf(seltext + sellen, "%%%02X",
                                                  (unsigned)*p & 0xFF);
                            }
                        }
                        seltext[sellen++] = '\r';
                        seltext[sellen++] = '\n';

                        free(resolved);

                        already_got_seltext = true;
                        break;
                      case 'o':
                        output_file_name = arg;
                        break;
                    }
                  }
                }
            }
        } else if (!strcmp(p, "--drop")) {
	    drop = true;
	    fork_when_writing = false;
        } else if (!strcmp(p, "--help")) {
	    usage();
	    return 0;
        } else if (!strcmp(p, "--version")) {
            version();
	    return 0;
        } else if (!strcmp(p, "--licence") || !strcmp(p, "--license")) {
            licence();
	    return 0;
	} else {
	    error ("unrecognised option `%s'", p);
	}
    }

    if (!reading && output_file_name)
        error("-o option not supported without -r");

    if (reading && input_file_name)
        error("input file name `%s' not supported with -r", input_file_name);

    if (drop && reading)
        error("drag-and-drop not supported with -r");

    if (!reading) {
	if (mode == TARGETS || mode == TIMESTAMP) {
	    error ("%s not supported in writing mode; use -r",
		   (mode == TARGETS ? "-t" :
		    /* mode == TIMESTAMP ? */ "-T"));
	}
    }

    if (mode == CUSTOM)
        convert_to_ctext = false; /* can't convert arbitrary things to
                                   * compound text */

    if (!reading && !already_got_seltext) {
        FILE *input_fp;

        if (input_file_name) {
            input_fp = fopen(input_file_name, "rb");
            if (!input_fp)
                error("%s: open: %s", input_file_name, strerror(errno));
        } else {
            input_fp = stdin;
        }

        seltext = malloc(sel_delta);
        if (!seltext)
            error ("out of memory");
        selsize = sel_delta;
        sellen = 0;
        do {
            n = fread(seltext+sellen, 1, selsize-sellen, input_fp);
            sellen += n;
            if (sellen >= selsize) {
                seltext = realloc(seltext, selsize += sel_delta);
                if (!seltext)
                    error ("out of memory");
            }
        } while (n > 0);
        if (sellen == selsize) {
            seltext = realloc(seltext, selsize += sel_delta);
            if (!seltext)
                error ("out of memory");
        }
        seltext[sellen] = '\0';

        if (input_file_name)
            fclose(input_fp);

        if (convert_utf16_utf8) {
            /* In selection-writing mode, expect UTF-8 on standard
             * input and convert it to UTF-16 to put in the
             * selection. */
            const char *ip;
            unsigned char *new_seltext = malloc(sellen * 2 + 4);
            unsigned char *op = new_seltext;

            /* UTF-16LE byte order mark */
            *op++ = 0xFF;
            *op++ = 0xFE;

            for (ip = seltext; ip < seltext+sellen ;) {
                unsigned val = (unsigned char)*ip++;
                int cbytes = (val < 0x80 ? 0 :
                              val < 0xC0 ? -1 :
                              val < 0xE0 ? 1 :
                              val < 0xF0 ? 2 :
                              val < 0xF8 ? 3 :
                              val < 0xFC ? 4 :
                              val < 0xFE ? 5 : -1);
                int i;

                if (cbytes < 0)
                    error("invalid UTF-8 lead byte 0x%02X", val);
                val &= (0x7F >> cbytes); /* strip off initial 1s */

                for (i = 0; i < cbytes; i++) {
                    if (ip >= seltext+sellen) {
                        error("UTF-8 input terminated with %d continuation "
                              "bytes outstanding", cbytes);
                    } else {
                        unsigned morebits = (unsigned char)*ip++;
                        if (morebits < 0x80 || morebits >= 0xC0) {
                            error("invalid UTF-8 continuation byte 0x%02X",
                                  morebits);
                        } else {
                            val = (val << 6) | (morebits & 0x3F);
                        }
                    }
                }

                if (cbytes && (val >> (cbytes > 1 ? 5*cbytes+1 : 7)) == 0)
                    error("overlong UTF-8 sequence for 0x%X", val);

                if (val < 0x10000) {
                    if (val >= 0xD800 && val < 0xE000) {
                        error("invalid Unicode character 0x%04X", val);
                    } else {
                        *op++ = val;
                        *op++ = val >> 8;
                    }
                } else if (val < 0x110000) {
                    unsigned hi = ((val - 0x10000) >> 10) + 0xD800;
                    unsigned lo = ((val - 0x10000) & 0x3FF) + 0xDC00;
                    *op++ = hi;
                    *op++ = hi >> 8;
                    *op++ = lo;
                    *op++ = lo >> 8;
                } else {
                    error("out-of-range Unicode character 0x%08X", val);
                }
            }

            sellen = (int)(op - new_seltext);
            *op++ = 0;
            *op++ = 0;

            free(seltext);
            seltext = (char *)new_seltext;
        }
    }

    eventloop = init_X();
    if (!reading && fork_when_writing) {
        /*
         * If we are writing the selection, we must go into the
         * background now.
         */
        int pid = fork();
        if (pid < 0) {
            error("unable to fork: %s", strerror(errno));
        } else if (pid > 0) {
            /*
             * we are the parent; just exit
             */
            return 0;
        }
        /*
         * we are the child
         */
        close(0);
        close(1);
        close(2);
        chdir("/");
    }
    if (eventloop)
        run_X();
    done_X();
    return 0;
}

/* handle errors */

void error (char *fmt, ...) {
    va_list ap;
    char errbuf[200];

    done_X();
    va_start (ap, fmt);
    vsprintf (errbuf, fmt, ap);
    va_end (ap);
    fprintf (stderr, "%s: %s", pname, errbuf);
    exit (1);
}

void open_output_file(void)
{
    if (output_fp)
        return;                        /* already opened */

    if (output_file_name) {
        output_fp = fopen(output_file_name, "wb");
        if (!output_fp)
            error("%s: open: %s", output_file_name, strerror(errno));
    } else {
        output_fp = stdout;
    }
}

/* begin the X interface */

char *lcasename = "xcopy";
char *ucasename = "XCopy";

Display *disp = NULL;
Window ourwin = None;
Atom compound_text_atom, targets_atom, timestamp_atom, atom_atom, integer_atom;
Atom multiple_atom, atom_pair_atom, incr_atom;
int screen, wwidth, wheight;

Atom strtype = XA_STRING;
Atom sel_atom = XA_PRIMARY;
Atom expected_type = (Atom)None;
int expected_format = 8;

Time sel_timestamp;

static const char *translate_atom(Display *disp, Atom atom)
{
    if (atom == None)
	return "None";
    else
	return XGetAtomName(disp, atom);
}

#define XDND_ATOM_LIST(X) \
    X(XdndSelection) \
    X(XdndAware) \
    X(XdndActionCopy) \
    X(XdndActionMove) \
    X(XdndActionLink) \
    X(XdndActionList) \
    X(XdndProxy) \
    X(XdndEnter) \
    X(XdndLeave) \
    X(XdndPosition) \
    X(XdndStatus) \
    X(XdndFinished) \
    X(XdndDrop)
#define DECLARE_ATOM(atom) Atom atom;
XDND_ATOM_LIST(DECLARE_ATOM)

/*
 * Returns true if we need to enter an event loop, false otherwise.
 */
bool init_X(void) {
    Window root;
    int x = 0, y = 0, width = 512, height = 128;
    int i, got = 0;
    XWMHints wm_hints;
    XSizeHints size_hints;
    XClassHint class_hints;
    XTextProperty textprop;
    XGCValues gcv;

    /* open the X display */
    disp = XOpenDisplay (display);
    if (!disp)
	error ("unable to open display");

    targets_atom = XInternAtom(disp, "TARGETS", false);
    timestamp_atom = XInternAtom(disp, "TIMESTAMP", false);
    atom_atom = XInternAtom(disp, "ATOM", false);
    atom_pair_atom = XInternAtom(disp, "ATOM_PAIR", false);
    multiple_atom = XInternAtom(disp, "MULTIPLE", false);
    integer_atom = XInternAtom(disp, "INTEGER", false);
    incr_atom = XInternAtom(disp, "INCR", false);
    if (mode == UTF8) {
	strtype = XInternAtom(disp, "UTF8_STRING", false);
    } else if (mode == HTML) {
	strtype = XInternAtom(disp, "text/html", false);
    } else if (mode == CTEXT) {
	strtype = XInternAtom(disp, "COMPOUND_TEXT", false);
    } else if (mode == TARGETS) {
	strtype = targets_atom;
	expected_type = atom_atom;
	expected_format = 32;
    } else if (mode == TIMESTAMP) {
	strtype = timestamp_atom;
	expected_type = integer_atom;
	expected_format = 32;
    } else if (mode == CUSTOM) {
	strtype = XInternAtom(disp, custom_seltype, false);
	expected_format = 0;
    }
    if (drop) {
#define INTERN_ATOM(atom) atom = XInternAtom(disp, #atom, false);
        do { XDND_ATOM_LIST(INTERN_ATOM) } while (0);
        sel_atom = XdndSelection;
    } else if (use_clipboard) {
        sel_atom = XInternAtom(disp, "CLIPBOARD", false);
    } else if (custom_selection) {
        sel_atom = XInternAtom(disp, custom_selection, false);
    }

    /* get the screen and root-window */
    screen = DefaultScreen (disp);
    root = RootWindow (disp, screen);

    x = y = 0;
    width = height = 10;	       /* doesn't really matter */

    /* actually create the window */
    {
        XSetWindowAttributes attrs;
        attrs.event_mask = PropertyChangeMask;
        ourwin = XCreateWindow(disp, root, x, y, width, height, 0,
                               CopyFromParent /* depth */,
                               CopyFromParent /* class */,
                               CopyFromParent /* visual */,
                               CWEventMask, &attrs);
    }

    /* resource class name */
    class_hints.res_name = lcasename;
    class_hints.res_class = ucasename;
    XSetClassHint (disp, ourwin, &class_hints);

    if (drop) {
        Font curfont;
        XColor fg, bg;
        Cursor cur;
        int grabret;
        Cardinal version = 5;
        Atom actions[3];

        curfont = XLoadFont(disp, "cursor");
        fg.red = fg.green = fg.blue = 0xFFFF;
        bg.red = bg.green = bg.blue = 0xFFFF;
        cur = XCreateGlyphCursor(disp, curfont, curfont, 34, 35, &fg, &bg);
        grabret = XGrabPointer(disp, root, false,
                               ButtonPressMask | PointerMotionMask,
                               GrabModeAsync, GrabModeAsync,
                               root, cur, CurrentTime);
        if (grabret != GrabSuccess) {
            error("unable to grab pointer for drag and drop");
        }

        XChangeProperty(disp, ourwin, XdndAware, atom_atom, 32,
                        PropModeReplace, (const unsigned char *)&version, 1);
        actions[0] = XdndActionCopy;
        actions[1] = XdndActionMove;
        actions[2] = XdndActionLink;
        XChangeProperty(disp, ourwin, XdndActionList, atom_atom, 32,
                        PropModeReplace, (const unsigned char *)&actions, 3);

        use_cutbuffers = false;
        still_grabbed = true;
    }

    /* do selection fiddling */
    if (reading) {
        /*
         * We are reading the selection. Call XConvertSelection to
         * request transmission of the selection data in the
         * appropriate format; the X event loop will then wait to
         * receive the data from the selection owner. Also we need to
         * make sure we receive PropertyNotify events, for INCR
         * transfers.
	 *
	 * If there is no selection owner, look in the cut buffer
	 * property on the root window.
         */
        if (XGetSelectionOwner(disp, sel_atom) != None) {
            Atom sel_property = XInternAtom(disp, "VT_SELECTION", false);
	    if (verbose)
		fprintf(stderr, "calling XConvertSelection: selection=%s"
			" target=%s property=%s requestor=%08lx\n",
			translate_atom(disp, sel_atom),
			translate_atom(disp, strtype),
			translate_atom(disp, sel_property),
			ourwin);
            XConvertSelection(disp, sel_atom, strtype,
                              sel_property, ourwin, CurrentTime);
            return true;
        } else if (use_cutbuffers) {
            /* No primary selection, so use the cut buffer. */
	    if (verbose)
		fprintf(stderr, "no selection owner: trying cut buffer\n");
            if (strtype == XA_STRING)
		do_paste(DefaultRootWindow(disp), XA_CUT_BUFFER0, true);
            return false;
        } else {
            /* Last fallback: do nothing. */
            return false;
        }
    } else {
        /*
         * Start by finding out the server time, to put in the
         * TIMESTAMP target of our selection. There's no GetServerTime
         * request in the X11 protocol, annoyingly (and amazingly!),
         * so we do this by the roundabout method of setting a useless
         * property on our own window (we use 'TIMESTAMP' as the
         * property name, which seems appropriate enough) and waiting
         * for the acknowledging PropertyNotify event which will have
         * a timestamp in it.
         */
        {
            static const unsigned char propval[] = {'d','u','m','m','y'};
            XEvent ev;

            XChangeProperty(disp, ourwin, timestamp_atom, XA_STRING, 8,
                            PropModeReplace, propval, sizeof(propval));

            do {
                XMaskEvent(disp, PropertyChangeMask, &ev);
            } while (ev.xproperty.state != PropertyNewValue);
            sel_timestamp = ev.xproperty.time;
        }

        /*
         * We are writing to the selection, so we establish ourselves
         * as selection owner.
         *
         * Also place the data in CUT_BUFFER0, if it isn't of an
         * exotic type (cut buffers can only take ordinary string
         * data, it turns out) or bigger than our maximum chunk size.
         */
        XSetSelectionOwner (disp, sel_atom, ourwin, sel_timestamp);
        if (XGetSelectionOwner (disp, sel_atom) != ourwin)
            error ("unable to obtain primary X selection");
        compound_text_atom = XInternAtom(disp, "COMPOUND_TEXT", false);
	if (strtype == XA_STRING && sellen <= sel_delta && use_cutbuffers) {
	    /*
	     * ICCCM-required cut buffer initialisation.
	     */
	    static const unsigned char emptystring[] = {0};
	    XChangeProperty(disp, root, XA_CUT_BUFFER0,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER1,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER2,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER3,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER4,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER5,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER6,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    XChangeProperty(disp, root, XA_CUT_BUFFER7,
			    XA_STRING, 8, PropModeAppend, emptystring, 0);
	    /*
	     * Rotate the cut buffers and add our text in CUT_BUFFER0.
	     */
	    XRotateBuffers(disp, 1);
	    XStoreBytes(disp, seltext, sellen);
	}
        return true;
    }
}

void write_data(Window requestor, Atom property, Atom type, int format,
                void *vdata, size_t size)
{
    int bformat = format / 8;          /* bytes per element */
    unsigned char *data = (unsigned char *)vdata;
    XEvent ev;

    if (!use_outgoing_incr || size * bformat <= sel_delta) {
	XChangeProperty(disp, requestor, property, type, format,
                        PropModeReplace, data, size);
    } else {
        /*
         * For large data, an incremental transfer as per ICCCM 2.7.2.
         */
        Cardinal totalsize = size * bformat;
        Cardinal sent, thissize;

        /*
         * Select for PropertyNotify events on the destination window,
         * so that we'll be able to know when to send the next chunk
         * of data.
         */
        XSelectInput(disp, requestor, PropertyChangeMask);

        /*
         * Start by sending a single 32-bit word with type INCR giving
         * the total size in bytes.
         */
	XChangeProperty(disp, requestor, property, incr_atom, 32,
                        PropModeReplace, (unsigned char *)&totalsize, 1);

        /*
         * Now set up an entry in our list of ongoing incremental
         * transfers, so that whenever that property is deleted, we'll
         * send the next batch.
         */
        if (nincrs >= incrsize) {
            incrsize = nincrs * 9 / 8 + 16;
            incrs = realloc(incrs, incrsize * sizeof(*incrs));
            if (!incrs)
                error ("out of memory");
        }
        incrs[nincrs].window = requestor;
        incrs[nincrs].property = property;
        incrs[nincrs].type = type;
        incrs[nincrs].format = format;
        incrs[nincrs].size = totalsize;
        incrs[nincrs].data = malloc(totalsize);
        if (!incrs[nincrs].data)
            error("out of memory");
        memcpy(incrs[nincrs].data, data, size);
        nincrs++;
    }
}

Atom convert_sel_inner(Window requestor, Atom target, Atom property) {
    if (target == strtype) {
	write_data(requestor, property, strtype, 8, seltext, sellen);
        pasted_something = true;
	return property;
    } else if (target == compound_text_atom && convert_to_ctext) {
	XTextProperty tp;
	XmbTextListToTextProperty(disp, &seltext, 1, XCompoundTextStyle, &tp);
	write_data(requestor, property, target, tp.format, tp.value,tp.nitems);
        pasted_something = true;
	return property;
    } else if (target == targets_atom) {
	Atom targets[16];
	int len = 0;
	targets[len++] = timestamp_atom;
	targets[len++] = targets_atom;
	targets[len++] = multiple_atom;
        /* Keep this in sync with enter_receiver */
	targets[len++] = strtype;
	if (strtype != compound_text_atom && convert_to_ctext)
	    targets[len++] = compound_text_atom;
	write_data(requestor, property, atom_atom, 32, targets, len);
	return property;
    } else if (target == timestamp_atom) {
	Time rettime = sel_timestamp;
	write_data(requestor, property, integer_atom, 32, &rettime, 1);
	return property;
    } else {
	return None;
    }
}

Atom convert_sel_outer(Window requestor, Atom target, Atom property) {
    /*
     * ICCCM 2.2 says that obsolete clients requesting the selection
     * request may not specify a property name under which they want
     * the data written to their window; selection owners are
     * encouraged to support such clients by reusing the selection
     * target name as the property.
     */
    if (property == None)
        property = target;

    if (target == multiple_atom) {
	/*
	 * Support for the MULTIPLE selection type, since it's
	 * specified as required in the ICCCM. Completely
	 * untested, though, because I have no idea of what X
	 * application might implement it for me to test against.
	 */

	int size = sel_delta;
	Atom actual_type;
	int actual_format, i;
	unsigned long nitems, bytes_after, nread;
	unsigned char *data;
	Atom *adata;

        if (property == (Atom)None)
            return None;               /* ICCCM says this isn't allowed */

	/*
	 * Fetch the requestor's window property giving a list of
	 * selection requests.
	 */
	while (XGetWindowProperty(disp, requestor, property, 0, size,
				  false, AnyPropertyType, &actual_type,
				  &actual_format, &nitems, &bytes_after,
				  (unsigned char **) &data) == Success &&
	       nitems * (actual_format / 8) == size) {
	    XFree(data);
	    size *= 3 / 2;
	}

	if (actual_type != atom_pair_atom || actual_format != 32) {
	    XFree(data);
	    return None;
	}

	adata = (Atom *)data;

	for (i = 0; i+1 < (long)nitems; i += 2) {
            bool failed = false;
            if (adata[i+1] == (Atom)None)   /* ICCCM says this isn't allowed */
                failed = true;
            else if (convert_sel_inner(requestor, adata[i],
                                       adata[i+1]) == (Atom)None)
                failed = true;

            if (failed) {
                /*
                 * There seems to be some disagreement over how you
                 * signal a failed target in MULTIPLE selection
                 * transfers. xterm zeroes out the _first_ atom of the
                 * pair in the main destination property, representing
                 * the target; GTK3 zeroes out the second one,
                 * representing the destination property for that
                 * target. My reading of ICCCM suggests the former,
                 * but the latter is more consistent with how a
                 * non-MULTIPLE selection is refused.
                 *
                 * For safety's sake, I'll therefore zero out _both_
                 * atoms!
                 */
                adata[i] = adata[i+1] = (Atom)None;
            }
	}

	XChangeProperty (disp, requestor, property,
			 atom_pair_atom, 32, PropModeReplace,
			 data, nitems);

	XFree(data);

	return property;
    } else {
        if (property == (Atom)None)
            property = target;      /* ICCCM says this is a sensible default */
	return convert_sel_inner(requestor, target, property);
    }
}

long xdnd_lookup_property(Window win, Atom property, long defaultval)
{
    long ret = defaultval;

    Atom actual_type;
    int actual_format;
    unsigned long nitems;
    unsigned long bytes_after;
    unsigned char *data;

    if (XGetWindowProperty(disp, win, property, 0, 1, false, AnyPropertyType,
                           &actual_type, &actual_format, &nitems,
                           &bytes_after, (unsigned char **)&data) == Success) {
        if (actual_format == 32 && nitems == 1) {
            ret = *((Cardinal *)data);
        }
        XFree(data);
    }
    return ret;
}

Window find_real_clientwin(Window w)
{
    static Atom wm_state_atom = None;
    static bool tried_wm_state = false;
    if (!tried_wm_state) {
        tried_wm_state = true;
        wm_state_atom = XInternAtom(disp, "WM_STATE", true);
    }
    if (!wm_state_atom) {
        /* If WM_STATE isn't even a known atom, then probably no WM is
         * running anyway, so this *is* the top-level sensible window. */
        return w;
    }

    {
        Atom actual_type;
        int actual_format;
        unsigned long nitems;
        unsigned long bytes_after;
        unsigned char *data;

        if (XGetWindowProperty(disp, w, wm_state_atom, 0, 1, false,
                               AnyPropertyType, &actual_type, &actual_format,
                               &nitems, &bytes_after, (unsigned char **)&data)
            == Success) {
            XFree(data);

            if (actual_type != None) {
                /* Found the proper client window. */
                return w;
            }
        }
    }

    {
        Window root, parent, *children, ret;
        unsigned nchildren;
        unsigned i;

        if (!XQueryTree(disp, w, &root, &parent, &children, &nchildren) ||
            !children)
            return None;               /* give up */

        ret = None;
        for (i = 0; i < nchildren; i++) {
            ret = find_real_clientwin(children[i]);
            if (ret != None)
                break;
        }

        XFree(children);

        return ret;
    }
}

void enter_receiver(Window recvclient)
{
    XEvent ev;
    Window proxy;

    /* Start by checking if the receiver has an XdndProxy property
     * set. */
    proxy = xdnd_lookup_property(recvclient, XdndProxy, recvclient);
    if (proxy != recvclient) {
        if (xdnd_lookup_property(proxy, XdndProxy, None) != proxy)
            proxy = recvclient;
    }

    /* Now see if the receiver or its proxy (as appropriate)
     * advertises XdndAware. */
    if (xdnd_lookup_property(proxy, XdndAware, 0) == 0)
        return;

    memset(&ev, 0, sizeof(ev));
    ev.type = ClientMessage;
    ev.xclient.window = proxy;
    ev.xclient.display = disp;
    ev.xclient.message_type = XdndEnter;
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = ourwin;
    ev.xclient.data.l[1] = 5 << 24;

    /* Data types offered: keep these in sync with convert_sel_inner */
    ev.xclient.data.l[2] = strtype;
    if (strtype != compound_text_atom && convert_to_ctext)
        ev.xclient.data.l[3] = compound_text_atom;

    XSendEvent (disp, ev.xclient.window, false, 0, &ev);
    drop_receiver = recvclient;
    drop_proxy = proxy;

    waiting_for_xdndstatus = false;
    last_xdndstatus_was_positive = false;
}

void leave_receiver(void)
{
    XEvent ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = ClientMessage;
    ev.xclient.window = drop_proxy;
    ev.xclient.display = disp;
    ev.xclient.message_type = XdndLeave;
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = ourwin;
    ev.xclient.data.l[1] = 0;
    XSendEvent (disp, ev.xclient.window, false, 0, &ev);
    drop_receiver = drop_proxy = None;
}

void run_X(void) {
    XEvent ev, e2;
    int i, j;

    while (1) {
	XNextEvent (disp, &ev);
        if (reading) {
            switch (ev.type) {
              case SelectionNotify:
		if (verbose)
		    fprintf(stderr, "got SelectionNotify: requestor=%08lx "
			    "selection=%s target=%s property=%s\n",
			    ev.xselection.requestor,
			    translate_atom(disp, ev.xselection.selection),
			    translate_atom(disp, ev.xselection.target),
			    translate_atom(disp, ev.xselection.property));

                if (ev.xselection.property != None)
                    do_paste(ev.xselection.requestor,
                             ev.xselection.property, false);
                return;
            }
        } else {
            bool have_ownership = true;

            switch (ev.type) {
              case SelectionClear:
                /* Selection has been cleared by another app. */
                have_ownership = false;
                break;
              case SelectionRequest:
                if (have_ownership) {
                    e2.xselection.type = SelectionNotify;
                    e2.xselection.requestor = ev.xselectionrequest.requestor;
                    e2.xselection.selection = ev.xselectionrequest.selection;
                    e2.xselection.target = ev.xselectionrequest.target;
                    e2.xselection.time = ev.xselectionrequest.time;
                    e2.xselection.property =
                        convert_sel_outer(ev.xselectionrequest.requestor,
                                          ev.xselectionrequest.target,
                                          ev.xselectionrequest.property);
                    XSendEvent (disp, ev.xselectionrequest.requestor,
                                false, 0, &e2);
                    if (oneshot && pasted_something)
                        return;
                }
                break;
              case PropertyNotify:
                for (i = j = 0; i < nincrs; i++) {
                    bool keep = true;
                    if (incrs[i].window == ev.xproperty.window &&
                        incrs[i].property == ev.xproperty.atom &&
                        ev.xproperty.state == PropertyDelete) {
                        size_t thissize = incrs[i].size;
                        if (thissize > sel_delta)
                            thissize = sel_delta;

                        XChangeProperty(disp,
                                        incrs[i].window, incrs[i].property,
                                        incrs[i].type, incrs[i].format,
                                        PropModeReplace, incrs[i].data,
                                        thissize / (incrs[i].format/8));

                        if (thissize == 0) {
                            /*
                             * If we've just sent a zero-length block,
                             * the incremental transfer is over and we
                             * should delete this entry.
                             */
                            keep = false;
                        }

                        incrs[i].data += thissize;
                        incrs[i].size -= thissize;
                    }
                    if (keep) {
                        if (j != i)
                            incrs[j] = incrs[i];
                        j++;
                    }
                }
                nincrs = j;

                /*
                 * If that was the last incremental transfer we
                 * had in flight for that destination window, we
                 * can stop receiving PropertyNotify events for it.
                 */
                for (i = 0; i < nincrs; i++)
                    if (incrs[i].window == ev.xproperty.window)
                        break;
                if (i == nincrs)
                    XSelectInput(disp, ev.xproperty.window, 0);

                break;
              case MotionNotify:
                if (drop && ev.xmotion.subwindow != None) {
                    Window child = ev.xmotion.subwindow;
                    Window receiver = find_real_clientwin(child);

                    /*
                     * Change drop receivers, if necessary.
                     */
                    if (drop_receiver != None && receiver != drop_receiver) {
                        leave_receiver();
                    }
                    if (receiver != None && receiver != drop_receiver) {
                        enter_receiver(receiver);
                    }

                    /*
                     * Send XdndPosition to our current receiver, if
                     * appropriate.
                     */
                    if (drop_receiver != None && receiver == drop_receiver &&
                        !waiting_for_xdndstatus) {
                        XEvent e2;
                        memset(&e2, 0, sizeof(e2));
                        e2.type = ClientMessage;
                        e2.xclient.window = drop_proxy;
                        e2.xclient.display = disp;
                        e2.xclient.message_type = XdndPosition;
                        e2.xclient.format = 32;
                        e2.xclient.data.l[0] = ourwin;
                        e2.xclient.data.l[1] = ev.xmotion.state;
                        e2.xclient.data.l[2] = ((ev.xmotion.x_root << 16) |
                                           ev.xmotion.y_root);
                        e2.xclient.data.l[3] = ev.xmotion.time;
                        e2.xclient.data.l[4] = XdndActionCopy;
                        XSendEvent (disp, e2.xclient.window, false, 0, &e2);
                        waiting_for_xdndstatus = true;
                    }
                }
                break;
              case ClientMessage:
                if (drop) {
                    if (ev.xclient.message_type == XdndStatus &&
                        drop_receiver != None &&
                        ev.xclient.data.l[0] == drop_receiver) {
                        last_xdndstatus_was_positive =
                            ev.xclient.data.l[1] & 1;
                        waiting_for_xdndstatus = false;
                    } else if (ev.xclient.message_type == XdndFinished) {
                        return;
                    }
                }
                break;
              case ButtonPress:
                if (drop && ev.xbutton.button >= 4 && ev.xbutton.button < 8) {
                    /* Buttons 4,5,6,7 cause a special XdndPosition
                     * indicating a scroll event, rather than
                     * terminating the drag. */
                    if (drop_receiver != None) {
                        XEvent e2;
                        memset(&e2, 0, sizeof(e2));
                        e2.type = ClientMessage;
                        e2.xclient.window = drop_proxy;
                        e2.xclient.display = disp;
                        e2.xclient.message_type = XdndPosition;
                        e2.xclient.format = 32;
                        e2.xclient.data.l[0] = ourwin;
                        e2.xclient.data.l[1] = (ev.xbutton.state | (1<<10) |
                                           ((ev.xbutton.button - 4) << 8));
                        e2.xclient.data.l[2] = ((ev.xbutton.x_root << 16) |
                                           ev.xbutton.y_root);
                        e2.xclient.data.l[3] = ev.xbutton.time;
                        e2.xclient.data.l[4] = XdndActionCopy;
                        XSendEvent (disp, e2.xclient.window, false, 0, &e2);
                    }
                } else if (drop && ev.xbutton.button < 4) {
                    /* Buttons 1-3 terminate the drag */
                    if (still_grabbed) {
                        XUngrabPointer(disp, ev.xbutton.time);
                        still_grabbed = false;
                    }
                    if (ev.xbutton.button == 1 &&
                        drop_receiver != None &&
                        last_xdndstatus_was_positive) {
                        XEvent e2;
                        memset(&e2, 0, sizeof(e2));
                        e2.type = ClientMessage;
                        e2.xclient.window = drop_proxy;
                        e2.xclient.display = disp;
                        e2.xclient.message_type = XdndDrop;
                        e2.xclient.format = 32;
                        e2.xclient.data.l[0] = ourwin;
                        e2.xclient.data.l[1] = 0;
                        e2.xclient.data.l[2] = ev.xbutton.time;
                        XSendEvent (disp, e2.xclient.window, false, 0, &e2);
                    } else {
                        if (drop_receiver != None)
                            leave_receiver();
                        return;
                    }
                }
                break;
            }
            if (nincrs == 0 && !have_ownership)
                return;
        }
    }
}

void done_X(void) {
    int i;

    if (ourwin != None)
	XDestroyWindow (disp, ourwin);
    if (disp)
	XCloseDisplay (disp);
}

void write_as_utf8(const void *data, int len)
{
    /*
     * This may receive the data in chunks with arbitrary boundaries,
     * so we maintain local state here in a bodgy set of static
     * variables. Most of them live inside the byte-by-byte loop, but
     * the top-level one says _whether_ we're doing encoding
     * conversion at all.
     */
    static enum { YES, NO, MAYBE } converting = MAYBE;

    if (!(len > 0))
        return;

    if (converting == MAYBE) {
        /* Decide whether this data is in UTF-16 at all, which we
         * figure out by looking at the first byte. If it's 0xFE
         * or 0xFF, i.e. an invalid UTF-8 byte, then we assume
         * it's half a BOM; otherwise, we assume the text is in
         * UTF-8 and doesn't need converting. */
        unsigned char first_byte = *(const unsigned char *)data;
        converting = (first_byte >= 0xFE) ? YES : NO;
    }

    if (converting == NO) {
        fwrite(data, 1, len, output_fp);
        return;
    }

    int i;
    for (i = 0; i < len; i++) {
        unsigned char byte = ((const unsigned char *)data)[i];

        static int shifts[2] = {0,8};
        static int whichbyte = 0;
        static unsigned surrogate = 0;
        static unsigned value = 0;

        value |= byte << shifts[whichbyte];
        if (++whichbyte == 2) {
            unsigned outval;

            whichbyte = 0;
            if (surrogate) {
                if (surrogate >= 0xD800 && surrogate < 0xDC00 &&
                    value >= 0xDC00 && value < 0xE000) {
                    outval = ((surrogate & 0x3FF) << 10) | (value & 0x3FF);
                    outval += 0x10000;
                } else {
                    outval = 0xFFFD;   /* surrogate error */
                }
                surrogate = 0;
            } else if (value >= 0xD800 && value < 0xDC00) {
                surrogate = value;
                goto done_output;      /* no output this time round */
            } else if (value >= 0xDC00 && value < 0xE000) {
                outval = 0xFFFD;       /* surrogate error */
            } else {
                outval = value;
            }

            if (outval == 0xFEFF) {
                goto done_output; /* BOM, already right way round */
            } else if (outval == 0xFFFE) {
                /* reversed BOM, so swap order */
                int tmp = shifts[0];
                shifts[0] = shifts[1];
                shifts[1] = tmp;
                goto done_output;
            }

            open_output_file();

            if (outval < (1U << 7)) {
                fputc(outval, output_fp);
            } else if (outval < (1U << (5+6))) {
                fputc(0xC0 |  (outval >> (1*6)),         output_fp);
                fputc(0x80 | ((outval         ) & 0x3F), output_fp);
            } else if (outval < (1U << (4+6+6))) {
                fputc(0xE0 |  (outval >> (2*6)),         output_fp);
                fputc(0x80 | ((outval >> (1*6)) & 0x3F), output_fp);
                fputc(0x80 | ((outval         ) & 0x3F), output_fp);
            } else if (outval < (1U << (3+6+6+6))) {
                fputc(0xF0 |  (outval >> (3*6)),         output_fp);
                fputc(0x80 | ((outval >> (2*6)) & 0x3F), output_fp);
                fputc(0x80 | ((outval >> (1*6)) & 0x3F), output_fp);
                fputc(0x80 | ((outval         ) & 0x3F), output_fp);
            }
            /* 5- and 6-byte sequences can't arise for UTF-16 input */

          done_output:
            value = 0;
        }
    }
}

void do_paste(Window window, Atom property, bool cutbuffer) {
    Atom actual_type;
    int actual_format, i;
    unsigned long nitems, bytes_after, nread;
    unsigned char *data;
    bool incremental = false;
    XEvent ev;

    nread = 0;
    while (XGetWindowProperty(disp, window, property, nread / 4, sel_delta / 4,
                              !cutbuffer, AnyPropertyType, &actual_type,
                              &actual_format, &nitems, &bytes_after,
                              (unsigned char **) &data) == Success) {
	if (verbose)
	    fprintf(stderr, "got %ld items of %d-byte data, type=%s;"
		    " %ld to go\n", nitems, actual_format,
		    translate_atom(disp, actual_type), bytes_after);

        /*
         * ICCCM 2.7.2: if we receive data with the type atom set to
         * INCR, it indicates that the actual data will arrive in
         * multiple chunks, terminating with a zero-length one.
         * Between each pair, we must wait for a PropertyNotify event
         * which tells us that the next chunk has arrived.
         */
        if (actual_type == incr_atom && !cutbuffer) {
            incremental = true;
            /*
             * Immediately wait for the first chunk of real data.
             */
            do {
                XMaskEvent(disp, PropertyChangeMask, &ev);
            } while (ev.xproperty.state != PropertyNewValue);
            /*
             * And loop straight back round to read it.
             */
            continue;
        }

	if (nitems > 0) {
	    /*
	     * We expect all returned chunks of data to be
	     * multiples of 4 bytes (because we can only request
	     * the subsequent starting offset in 4-byte
	     * increments). Of course you can store an odd number
	     * of bytes in a selection, so this can't be the case
	     * every time XGetWindowProperty returns; but it
	     * should be the case every time it returns _and there
	     * is more data to come_.
	     *
	     * Hence, whenever XGetWindowProperty returns, we
	     * verify that the size of the data returned _last_
	     * time was divisible by 4.
	     */
	    if ((nread & 3) != 0) {
		error("unexpected data size: %d read (not a multiple"
		      " of 4), but more to come", nread);
	    }

	    if (expected_type != (Atom)None && actual_type != expected_type) {
		const char *expout = translate_atom(disp, expected_type);
		const char *gotout = translate_atom(disp, actual_type);
		error("unexpected data type: expected %s, got %s",
		      expout, gotout);
	    }
	    if (expected_format && expected_format != actual_format) {
		error("unexpected data format: expected %d-bit, got %d-bit",
		      expected_format, actual_format);
	    }
	}

        if (nitems > 0) {
            open_output_file();
	    if (mode == TARGETS) {
		assert(actual_format == 32);
		int i;
		for (i = 0; i < nitems; i++) {
		    Atom x = ((Atom *)data)[i];
		    fprintf(output_fp, "%s\n", translate_atom(disp, x));
		}
	    } else if (mode == TIMESTAMP) {
		assert(actual_format == 32);
		Time x = ((Time *)data)[0];
		fprintf(output_fp, "%"PRIu32"\n", (uint32_t)x);
	    } else {
                if (convert_utf16_utf8) {
                    write_as_utf8(data, nitems * actual_format / 8);
                } else {
                    fwrite(data, actual_format / 8, nitems, output_fp);
                }
		nread += nitems * actual_format / 8;
	    }
        }
        XFree(data);
	if (bytes_after == 0) {
            /*
             * We've come to the end of the property we're reading.
             */
            if (incremental) {
                /*
                 * For an incremental transfer, this means we wait for
                 * another property to be dumped in the same place on
                 * our window, and loop round again reading that. The
                 * exception is if the total length of the property we
                 * got was zero, which signals the end.
                 */
                if (nread == 0 && nitems == 0)
                    break;             /* all done */

                /* otherwise wait for the next chunk */
                do {
                    XMaskEvent(disp, PropertyChangeMask, &ev);
                } while (ev.xproperty.state != PropertyNewValue);

                /* which we read from the beginning */
                nread = 0;
            } else {
		break;
            }
	}
    }
}
